{
  description = "My NixOS Configuration";

  inputs = {
    nur.url = "github:nix-community/NUR";
    neovim-nightly.url = "github:nix-community/neovim-nightly-overlay";

    sops-nix.url = "github:Mic92/sops-nix";
    nix-colors.url = "github:Misterio77/nix-colors";
    home-manager.url = "github:nix-community/home-manager";

    master.url = "github:nixos/nixpkgs/master";
    unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    stable.url = "github:nixos/nixpkgs/nixos-21.11";

    deploy-rs.url = "github:serokell/deploy-rs";
  };

  outputs =
    { self
    , home-manager
    , neovim-nightly
    , nur
    , sops-nix
    , nix-colors
    , master
    , stable
    , unstable
    , deploy-rs
    , ...
    }@inputs:
    let
    in
    {

      packages.x86_64-linux.get-apikey = let 
        pkgs = import unstable { system = "x86_64-linux"; };
      in pkgs.writeShellScriptBin "get-apikey" ''
        ssh $1@$2 'nix-shell -p headscale --run "headscale apikeys create"'
      '';

      nixosConfigurations = import ./nixos {
        inherit self stable unstable master;
        inherit home-manager sops-nix nix-colors;
        inherit nur neovim-nightly; 

        nixpkgs = unstable;
      };

      deploy.nodes.arceus = {
        hostname = "";
        profiles.system = {
          sshUser = "root";
          user = "root";
          path = deploy-rs.lib.x86_64-linux.activate.nixos self.nixosConfigurations.arceus;
        };
      };

      checks = builtins.mapAttrs (system: deployLib: deployLib.deployChecks self.deploy) deploy-rs.lib;

      # Shorthand
      shiggy = self.nixosConfigurations.shiggy.config.system.build.toplevel;
    };
}
