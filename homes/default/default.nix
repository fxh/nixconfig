{ config, colorscheme, ... }:

{
  inherit colorscheme;

  imports = [
    ./misc
    ./programs
    ./services
  ];

  targets.genericLinux.enable = true;
  systemd.user.startServices = "sd-switch";

  home.stateVersion = "21.11";
}
