{ ... }:

{
  imports = [
    ./font.nix
    ./home.nix
    ./profile.nix
    ./xdg.nix
    ./gtk.nix
    ./packages.nix
  ];
}
