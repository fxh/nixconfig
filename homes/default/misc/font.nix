{ ... }:

{
  font = {
    family = "Iosevka Nerd Font Mono";
    size = {
      tiny = "6";
      small = "8";
      normal = "10";
      big = "16";
      huge = "20";
    };
  };
}


