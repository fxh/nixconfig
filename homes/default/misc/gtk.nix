{ config, pkgs, nix-colors, ... }:
let
  inherit (nix-colors.lib-contrib { inherit pkgs; }) gtkThemeFromScheme;

in
{
  gtk = {
    enable = true;
    iconTheme = {
      name = "papirus-icon-theme";
      package = pkgs.papirus-icon-theme;
    };
    theme = {
      name = "${config.colorscheme.slug}";
      package = gtkThemeFromScheme {
        scheme = config.colorscheme;
      };
    };
  };
}

