{ config, name, ... }:

{
  home.username = name;
  home.homeDirectory = "/home/${name}";

  home.sessionPath = [
    "${config.home.homeDirectory}/.nix-profile/bin"
    "${config.home.homeDirectory}/${config.programs.go.goBin}"
    "/nix/var/nix/profiles/default/bin"
  ];

  home.sessionVariables = {
    EDITOR = "${config.programs.neovim.finalPackage}/bin/nvim";
  };
}
