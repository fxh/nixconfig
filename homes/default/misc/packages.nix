{ pkgs, ... }:

{
  home.packages = with pkgs; [
    airshipper
    lutris
    gnome.adwaita-icon-theme
    nixpkgs-fmt
    wtype
    wl-clipboard
    ripgrep
    fd
    jetbrains.idea-community
    android-studio
  ];
}
