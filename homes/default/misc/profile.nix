{ config, ... }: 

{
  profile = {
    sshKey = {
      name = "fhilgers@yubikey";
      pub = "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIELXIFPGB9jELp7Fzlq2ujHwAOw9p4ktVpLKcc4ghNucAAAABHNzaDo=";
    };
    u2fKey = "${config.home.username}:*,wiyykJ1T7fA4snFjO76CY5503FI+jTHxHSwXY6l2+FM=,eddsa,+presence";
    name = "Felix Hilgers";
    email = "felix_hilgers@posteo.de";
  };
}
