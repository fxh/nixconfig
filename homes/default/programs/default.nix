{ ... }:

{
  imports = [
    ./bat.nix
    ./bottom.nix
    ./direnv.nix
    ./exa.nix
    ./firefox.nix
    ./foot.nix
    ./git.nix
    ./go.nix
    ./home-manager.nix
    ./neovim.nix
    ./starship.nix
    ./swaylock.nix
    ./vivid.nix
    ./zoxide.nix
    ./zsh.nix
  ];
}
