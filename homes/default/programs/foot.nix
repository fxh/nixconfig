{ config, pkgs, ... }:

{
  programs.foot = {
    enable = true;

    server.enable = true;

    settings = {
      main = {
        app-id = "foot";
        title = "foot";
        font = "${config.font.family}:size=${config.font.size.normal}";
      };

      bell = {
        urgent = "no";
        notify = "no";
      };

      scrollback = {
        lines = 10000;
        multiplier = 4.0;
      };

      url = {
        launch = "${pkgs.xdg-utils}/bin/xdg-open \${url}";
      };

      mouse = {
        hide-when-typing = true;
      };

      colors = with config.colorscheme.colors; {
        foreground = "${base05}";
        background = "${base00}";

        regular0 = "${base00}";
        regular1 = "${base08}";
        regular2 = "${base0B}";
        regular3 = "${base0A}";
        regular4 = "${base0D}";
        regular5 = "${base0E}";
        regular6 = "${base0C}";
        regular7 = "${base05}";

        bright0 = "${base03}";
        bright1 = "${base09}";
        bright2 = "${base01}";
        bright3 = "${base02}";
        bright4 = "${base04}";
        bright5 = "${base06}";
        bright6 = "${base0F}";
        bright7 = "${base07}";

        selection-foreground = "${base00}";
        selection-background = "${base05}";
      };
    };
  };
}
