{ config, pkgs, ... }:

let
  signersFilePath = "${pkgs.writeText "allowed_signers" ''
    ${config.profile.sshKey.name} ${config.profile.sshKey.pub}
  ''}";

  keyFilePath = "${pkgs.writeText "id_key" ''
    ${config.profile.sshKey.pub} ${config.profile.sshKey.name}
  ''}";

in {
  programs.git = {
    enable = true;
    userName = config.profile.name;
    userEmail = config.profile.email;
    signing = {
      key = keyFilePath;
      signByDefault = true;
      gpgPath = "";
    };
    extraConfig = {
      gpg = {
        format = "ssh";
        ssh.allowedSignersFile = signersFilePath;
      };
    };
  };
}
