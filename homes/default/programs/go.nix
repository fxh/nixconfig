{ config, ... }:

{
  programs.go = {
    enable = true;
    goPath = ".go";
    goBin = "${config.programs.go.goPath}/bin";
  };
}
