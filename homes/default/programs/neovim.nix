{ config, pkgs, ... }:

{
  programs.neovim = {
    enable = true;
    package = pkgs.neovim-nightly;
    extraConfig = builtins.concatStringsSep "\n" [
      ''
        lua << EOF
        local g = vim.g
        local opt = vim.opt

        -- Leader
        g.mapleader = ' '

        -- Performance
        opt.lazyredraw = true;
        opt.shell = "zsh"
        opt.shadafile = "NONE"

        -- Colors 
        opt.termguicolors = true

        -- Undo files
        opt.undofile = true

        -- Indentation
        opt.smartindent = true
        opt.tabstop = 4
        opt.shiftwidth = 4
        opt.shiftround = true
        opt.expandtab = true
        opt.scrolloff = 3

        -- Set clipboard to use system clip
        opt.clipboard = "unnamedplus"

        -- Use mouse
        opt.mouse = "a"

        -- UI
        opt.cursorline = true
        opt.relativenumber = true
        opt.number = true

        -- Yeet viminfo file
        opt.viminfo = ""
        opt.viminfofile = "NONE"

        -- Misc
        opt.ignorecase = true
        opt.ttimeoutlen = 5
        opt.compatible = false
        opt.hidden = true
        opt.shortmess = "atI"
        opt.wrap = false
        opt.backup = false
        opt.writebackup = false
        opt.errorbells = false
        opt.swapfile = false
        opt.showmode = false

        vim.diagnostic.config({
          virtual_text = true,
          signs = true,
          underline = true,
          update_in_insert = false,
          severity_sort = true,
        })
        
        local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
        for type, icon in pairs(signs) do
          local hl = "DiagnosticSign" .. type
          vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
        end

        vim.cmd [[autocmd! ColorScheme * highlight NormalFloat guibg=#1f2335]]
        vim.cmd [[autocmd! ColorScheme * highlight FloatBorder guifg=white guibg=#1f2335]]

        local border = {
              {"🭽", "FloatBorder"},
              {"▔", "FloatBorder"},
              {"🭾", "FloatBorder"},
              {"▕", "FloatBorder"},
              {"🭿", "FloatBorder"},
              {"▁", "FloatBorder"},
              {"🭼", "FloatBorder"},
              {"▏", "FloatBorder"},
        }

        -- LSP settings (for overriding per client)
        local handlers =  {
          ["textDocument/hover"] =  vim.lsp.with(vim.lsp.handlers.hover, {border = border}),
          ["textDocument/signatureHelp"] =  vim.lsp.with(vim.lsp.handlers.signature_help, {border = border }),
        }

        -- To instead override globally
        local orig_util_open_floating_preview = vim.lsp.util.open_floating_preview
        function vim.lsp.util.open_floating_preview(contents, syntax, opts, ...)
          opts = opts or {}
          opts.border = opts.border or border
          return orig_util_open_floating_preview(contents, syntax, opts, ...)
        end
          
        EOF
      ''
    ];
    extraPackages = with pkgs; [
      tree-sitter
      nodejs
      zig
      fd
      bat
      ripgrep
      efm-langserver
      python310Packages.flake8
      python310Packages.black
      pyright
      gopls
      rnix-lsp
      nodePackages.typescript-language-server
      clang-tools
      cmake-language-server
    ];
    plugins = with pkgs.vimPlugins; [
      { plugin = vim-nix; }
      { plugin = vim-bbye; }
      { plugin = nvim-cmp; 
        type = "lua";
        config = ''
          -- nvim-cmp setup
          local luasnip = require('luasnip')
          local cmp_autopairs = require('nvim-autopairs.completion.cmp')
          local cmp = require('cmp')
          cmp.event:on('confirm_done', cmp_autopairs.on_confirm_done({ map_char = { tex = ''' }}))
          cmp.setup {
            snippet = {
              expand = function(args)
                require('luasnip').lsp_expand(args.body)
              end,
            },
            mapping = {
              ['<C-p>'] = cmp.mapping.select_prev_item(),
              ['<C-n>'] = cmp.mapping.select_next_item(),
              ['<C-d>'] = cmp.mapping.scroll_docs(-4),
              ['<C-f>'] = cmp.mapping.scroll_docs(4),
              ['<C-Space>'] = cmp.mapping.complete(),
              ['<C-e>'] = cmp.mapping.close(),
              ['<CR>'] = cmp.mapping.confirm {
                behavior = cmp.ConfirmBehavior.Replace,
                select = true,
              },
              ['<Tab>'] = function(fallback)
                if cmp.visible() then
                  cmp.select_next_item()
                elseif luasnip.expand_or_jumpable() then
                  luasnip.expand_or_jump()
                else
                  fallback()
                end
              end,
              ['<S-Tab>'] = function(fallback)
                if cmp.visible() then
                  cmp.select_prev_item()
                elseif luasnip.jumpable(-1) then
                  luasnip.jump(-1)
                else
                  fallback()
                end
              end,
            },
            sources = {
              { name = 'nvim_lsp' },
              { name = 'luasnip' },
              { name = 'neorg' },
            },
          }
        '';
      }
      { plugin = cmp_luasnip; }
      { plugin = luasnip; }
      { plugin = dhall-vim; }
      { plugin = nvim-treesitter; 
        type = "lua";
        config = ''
          local parser_configs = require("nvim-treesitter.parsers").get_parser_configs()

          parser_configs.norg = {
              install_info = {
                  url = "https://github.com/nvim-neorg/tree-sitter-norg",
                  files = { "src/parser.c", "src/scanner.cc" },
                  branch = "main",
              },
          }

          parser_configs.norg_meta = {
              install_info = {
                  url = "https://github.com/nvim-neorg/tree-sitter-norg-meta",
                  files = { "src/parser.c" },
                  branch = "main",
              },
          }

          parser_configs.norg_table = {
              install_info = {
                  url = "https://github.com/nvim-neorg/tree-sitter-norg-table",
                  files = { "src/parser.c" },
                  branch = "main",
              },
          }
          require'nvim-treesitter.configs'.setup {
            ensure_installed = { "norg", "norg_meta", "norg_table" },
            sync_install = false,
            highlight = {
              enable = true,
            },
            parser_install_dir = "~/.config/nvim/parsers",
          }
        '';
      }

      { plugin = which-key-nvim;
        type = "lua";
        config = ''
          require'which-key'.setup {}
        '';
      }
      {
        plugin = lsp_signature-nvim;
        type = "lua";
        config = ''
          require'lsp_signature'.setup({})
        '';
      }
      {
        plugin = toggleterm-nvim;
        type = "lua";
        config = ''
          require'toggleterm'.setup{
            open_mapping = [[<C-\>]],
            float_opts = {
              winblend = 0,
            },
            direction = 'float',
          }
          function _G.set_terminal_keymaps()
            local opts = {noremap = true}
            vim.api.nvim_buf_set_keymap(0, 't', '<esc>', [[<C-\><C-n>]], opts)
            vim.api.nvim_buf_set_keymap(0, 't', 'jk', [[<C-\><C-n>]], opts)
            vim.api.nvim_buf_set_keymap(0, 't', '<C-h>', [[<C-\><C-n><C-W>h]], opts)
            vim.api.nvim_buf_set_keymap(0, 't', '<C-j>', [[<C-\><C-n><C-W>j]], opts)
            vim.api.nvim_buf_set_keymap(0, 't', '<C-k>', [[<C-\><C-n><C-W>k]], opts)
            vim.api.nvim_buf_set_keymap(0, 't', '<C-l>', [[<C-\><C-n><C-W>l]], opts)
          end

          -- if you only want these mappings for toggle term use term://*toggleterm#* instead
          vim.cmd('autocmd! TermOpen term://* lua set_terminal_keymaps()')
        '';
      }
      {
        plugin = indent-blankline-nvim;
        type = "lua";
        config = ''
            require'indent_blankline'.setup {}
        '';
      }
      { plugin = gitsigns-nvim; 
        type = "lua";
        config = ''
          require('gitsigns').setup {
            signcolumn = true,
            word_diff = true,
            current_line_blame = true,
          }
        '';
      }
      { plugin = nvim-gps; }
      { 
        plugin = feline-nvim; 
        type = "lua";
        config = with config.colorscheme.colors; ''
          vim.opt.termguicolors = true
          local colors = {
            bg = '#${base01}',
            black = '#${base01}',
            fg = '#${base05}',
            white = '#${base05}',
            yellow = '#${base0A}',
            cyan = '#${base0C}',
            skycyan = '#${base0C}',
            green = '#${base0B}',
            orange = '#${base09}',
            violet = '#${base0E}',
            magenta = '#${base0E}',
            blue = '#${base0D}',
            oceanblue = '#${base04}',
            red = '#${base08}'
          }
          local lsp = require('feline.providers.lsp')
          local vi_mode_utils = require('feline.providers.vi_mode')
          local gps = require("nvim-gps")

          local force_inactive = {
            filetypes = {},
            buftypes = {},
            bufnames = {}
          }

          local components = {
            active = {{}, {}, {}},
            inactive = {{}, {}, {}},
          }

          local vi_mode_colors = {
            NORMAL = 'green',
            OP = 'green',
            INSERT = 'red',
            VISUAL = 'skyblue',
            LINES = 'skyblue',
            BLOCK = 'skyblue',
            REPLACE = 'violet',
            ['V-REPLACE'] = 'violet',
            ENTER = 'cyan',
            MORE = 'cyan',
            SELECT = 'orange',
            COMMAND = 'green',
            SHELL = 'green',
            TERM = 'green',
            NONE = 'yellow'
          }

          local vi_mode_text = {
            NORMAL = '❰',
            OP = '❰',
            INSERT = '❱',
            VISUAL = '❰❱',
            LINES = '❰❱',
            BLOCK = '❰❱',
            REPLACE = '❰❱',
            ['V-REPLACE'] = '❰❱',
            ENTER = '❰❱',
            MORE = '❰❱',
            SELECT = '❰❱',
            COMMAND = '❮',
            SHELL = '❮',
            TERM = '❮',
            NONE = '❰❱'
          }

          local buffer_not_empty = function()
            if vim.fn.empty(vim.fn.expand('%:t')) ~= 1 then
              return true
            end
            return false
          end

          local checkwidth = function()
            local squeeze_width  = vim.fn.winwidth(0) / 2
            if squeeze_width > 40 then
              return true
            end
            return false
          end

          force_inactive.filetypes = {
            'NvimTree',
            'dbui',
            'packer',
            'startify',
            'fugitive',
            'fugitiveblame'
          }

          force_inactive.buftypes = {
            'terminal'
          }

          -- LEFT

          -- vi-symbol
          components.active[1][1] = {
            provider = function()
              return vi_mode_text[vi_mode_utils.get_vim_mode()]
            end,
            hl = function()
              local val = {}
              val.fg = vi_mode_utils.get_mode_color()
              val.bg = 'bg'
              val.style = 'bold'
              return val
            end,
            right_sep = ' '
          }
          -- filename
          components.active[1][2] = {
            provider = function()
              return vim.fn.expand("%:F")
            end,
            hl = {
              fg = 'white',
              bg = 'bg',
              style = 'bold'
            },
            right_sep = {
              str = ' > ',
              hl = {
                fg = 'white',
                bg = 'bg',
                style = 'bold'
              },
            }
          }
          -- nvimGps
          components.active[1][3] = {
            provider = function() return gps.get_location() end,
            enabled = function() return gps.is_available() end,
            hl = {
              fg = 'white',
              bg = 'bg',
              style = 'bold'
            }
          }

          -- gitBranch
          components.active[1][4] = {
            provider = 'git_branch',
            hl = {
              fg = 'yellow',
              bg = 'bg',
              style = 'bold'
            }
          }
          -- diffAdd
          components.active[1][5] = {
            provider = 'git_diff_added',
            hl = {
              fg = 'green',
              bg = 'bg',
              style = 'bold'
            }
          }
          -- diffModfified
          components.active[1][6] = {
            provider = 'git_diff_changed',
            hl = {
              fg = 'orange',
              bg = 'bg',
              style = 'bold'
            }
          }
          -- diffRemove
          components.active[1][7] = {
            provider = 'git_diff_removed',
            hl = {
              fg = 'red',
              bg = 'bg',
              style = 'bold'
            },
          }
          -- diagnosticErrors
          components.active[1][8] = {
            provider = 'diagnostic_errors',
            enabled = function() return lsp.diagnostics_exist(vim.diagnostic.severity.ERROR) end,
            hl = {
              fg = 'red',
              style = 'bold'
            }
          }
          -- diagnosticWarn
          components.active[1][9] = {
            provider = 'diagnostic_warnings',
            enabled = function() return lsp.diagnostics_exist(vim.diagnostic.severity.WARN) end,
            hl = {
              fg = 'yellow',
              style = 'bold'
            }
          }
          -- diagnosticHint
          components.active[1][10] = {
            provider = 'diagnostic_hints',
            enabled = function() return lsp.diagnostics_exist(vim.diagnostic.severity.HINT) end,
            hl = {
              fg = 'cyan',
              style = 'bold'
            }
          }
          -- diagnosticInfo
          components.active[1][11] = {
            provider = 'diagnostic_info',
            enabled = function() return lsp.diagnostics_exist(vim.diagnostic.severity.INFO) end,
            hl = {
              fg = 'skyblue',
              style = 'bold'
            },
          }
          -- diagnosticInfo
          components.active[1][12] = {
            provider = ' ',
            hl = {
              fg = 'skyblue',
              style = 'bold'
            },
          }

          -- RIGHT

          -- LspName
          components.active[3][1] = {
            provider = 'lsp_client_names',
            hl = {
              fg = 'yellow',
              bg = 'bg',
              style = 'bold'
            },
            right_sep = ' '
          }
          -- fileIcon
          components.active[3][2] = {
            provider = function()
              local filename = vim.fn.expand('%:t')
              local extension = vim.fn.expand('%:e')
              local icon  = require'nvim-web-devicons'.get_icon(filename, extension)
              if icon == nil then
                icon = ''
              end
              return icon
            end,
            hl = function()
              local val = {}
              local filename = vim.fn.expand('%:t')
              local extension = vim.fn.expand('%:e')
              local icon, name  = require'nvim-web-devicons'.get_icon(filename, extension)
              if icon ~= nil then
                val.fg = vim.fn.synIDattr(vim.fn.hlID(name), 'fg')
              else
                val.fg = 'white'
              end
              val.bg = 'bg'
              val.style = 'bold'
              return val
            end,
            right_sep = ' '
          }
          -- fileType
          components.active[3][3] = {
            provider = 'file_type',
            hl = function()
              local val = {}
              local filename = vim.fn.expand('%:t')
              local extension = vim.fn.expand('%:e')
              local icon, name  = require'nvim-web-devicons'.get_icon(filename, extension)
              if icon ~= nil then
                val.fg = vim.fn.synIDattr(vim.fn.hlID(name), 'fg')
              else
                val.fg = 'white'
              end
              val.bg = 'bg'
              val.style = 'bold'
              return val
            end,
            right_sep = ' '
          }
          -- fileSize
          components.active[3][4] = {
            provider = 'file_size',
            enabled = function() return vim.fn.getfsize(vim.fn.expand('%:t')) > 0 end,
            hl = {
              fg = 'skyblue',
              bg = 'bg',
              style = 'bold'
            },
            right_sep = ' '
          }
          -- fileFormat
          components.active[3][5] = {
            provider = function() return ''' .. vim.bo.fileformat:upper() .. ''' end,
            hl = {
              fg = 'white',
              bg = 'bg',
              style = 'bold'
            },
            right_sep = ' '
          }
          -- fileEncode
          components.active[3][6] = {
            provider = 'file_encoding',
            hl = {
              fg = 'white',
              bg = 'bg',
              style = 'bold'
            },
            right_sep = ' '
          }
          -- lineInfo
          components.active[3][7] = {
            provider = 'position',
            hl = {
              fg = 'white',
              bg = 'bg',
              style = 'bold'
            },
            right_sep = ' '
          }
          -- linePercent
          components.active[3][8] = {
            provider = 'line_percentage',
            hl = {
              fg = 'white',
              bg = 'bg',
              style = 'bold'
            },
            right_sep = ' '
          }
          -- scrollBar
          components.active[3][9] = {
            provider = 'scroll_bar',
            hl = {
              fg = 'yellow',
              bg = 'bg',
            },
          }

          -- INACTIVE

          -- fileType
          components.inactive[1][1] = {
            provider = 'file_type',
            hl = {
              fg = 'black',
              bg = 'cyan',
              style = 'bold'
            },
            left_sep = {
              str = ' ',
              hl = {
                fg = 'NONE',
                bg = 'cyan'
              }
            },
            right_sep = {
              {
                str = ' ',
                hl = {
                  fg = 'NONE',
                  bg = 'cyan'
                }
              },
              ' '
            }
          }

          require('feline').setup({
            theme = colors,
            default_bg = bg,
            default_fg = fg,
            vi_mode_colors = vi_mode_colors,
            components = components,
            force_inactive = force_inactive,
          })
        '';
      }
      {
        plugin = bufferline-nvim;
        type = "lua";
        config = ''
          require('bufferline').setup{}
        '';
      }
      { plugin = nvim-web-devicons; }
      { plugin = nvim-autopairs;
        type = "lua";
        config = ''
          require'nvim-autopairs'.setup()
        '';
      }
      { 
        plugin = nvim-tree-lua; 
        type = "lua";
        config = ''
          require'nvim-tree'.setup {
            open_on_setup = true,
            auto_reload_on_write = true,
            open_on_tab = true,
          }
        '';
      }
      {
        plugin = telescope-nvim;
        type = "lua";
        config = ''
          local setup_telescope = function()

            local builtin = require('telescope.builtin')

            require'which-key'.register({
              f = {
                name = 'files',
                f = { builtin.find_files, 'Find Files' },
                g = { builtin.live_grep, 'Grep Files' },
                t = { '<cmd>NvimTreeToggle<cr>', 'Toggle File Viewer' },
              },
              b = {
                name = 'buffer',
                b = { builtin.buffers, 'Switch Buffer' },
                n = { '<cmd>bnext<cr>', 'Next Buffer' },
                N = { '<cmd>enew<cr>', 'New Buffer' },
                p = { '<cmd>bprevious<cr>', 'Previous Buffer' },
                d = { '<cmd>Bwipeout<cr>', 'Delete Buffer' },
                s = { '<cmd>w<cr>', 'Save Buffer' },
                S = { '<cmd>wa<cr>', 'Save all Buffers' },
              },
              w = {
                name = 'window',
                w = { '<C-w>w', '''},
                j = { '<C-w>j', '''},
                h = { '<C-w>h', '''},
                k = { '<C-w>k', '''},
                l = { '<C-w>l', '''},
                J = { '<C-w>J', '''},
                H = { '<C-w>H', '''},
                K = { '<C-w>K', '''},
                L = { '<C-w>L', '''},
                s = { '<C-w>s', '''},
                d = { '<C-w>q', '''},
              },
              h = {
                name = 'help',
                h = { builtin.help_tags, 'Find help' },
                r = { '<cmd>source $MYVIMRC<cr>', 'Reload Config' },
              }
            }, { prefix = '<leader>' })

          end

          setup_telescope()
        '';
      }
      {
        plugin = cmp-nvim-lsp;
        type = "lua";
        config = ''
          local capabilities = vim.lsp.protocol.make_client_capabilities()
          capabilities = require('cmp_nvim_lsp').update_capabilities(capabilities)
        '';
      }
      {
        plugin = nvim-lspconfig;
        type = "lua";
        config = ''
          local nvim_lsp = require('lspconfig')

          -- Use an on_attach function to only map the following keys
          -- after the language server attaches to the current buffer
          local on_attach = function(client, bufnr)
            local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
            local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

            -- Enable completion triggered by <c-x><c-o>
            buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

            -- Mappings.
            local opts = { noremap=true, silent=true }

            -- See `:help vim.lsp.*` for documentation on any of the below functions
            buf_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
            buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
            buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
            buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
            buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
            buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
            buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
            buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
            buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
            buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
            buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
            buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
            buf_set_keymap('n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
            buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
            buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
            buf_set_keymap('n', '<space>pq', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
            buf_set_keymap('n', '<space>pf', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)

          end

          -- Use a loop to conveniently call 'setup' on multiple servers and
          -- map buffer local keybindings when the language server attaches
          -- Enable some language servers with the additional completion capabilities offered by nvim-cmp
          local servers = { 'gopls', 'rnix', 'tsserver', 'pyright', 'clangd', 'cmake' }
          for _, lsp in ipairs(servers) do
            nvim_lsp[lsp].setup {
              on_attach = on_attach,
              -- on_attach = my_custom_on_attach,
              capabilities = capabilities,
              flags = {
                debounce_text_changes = 150,
              }
            }
          end

          nvim_lsp['efm'].setup {
            on_attach = on_attach,
            capabilities = capabilities,
            init_options = {documentFormatting=true},
            flags = {
              debounce_text_changes = 150,
            },
            settings = {
              rootMarkers = { ".git/" },
              languages = {
                python = {
                  { formatCommand = "black --quiet -", formatStdin = true },
                  {
                    lintCommand = "flake8 --stdin-display-name ''${INPUT} -",
                    lintStdin = true,
                    lintFormats = { '%f:%l:%c: %t%n %m'},
                    lintCategoryMap = {
                      E = "E",
                      F = "E",
                      W = "W",
                    },
                  },
                },
              },
            },
            filetypes = { 'python' },
          }

          -- Set completeopt to have a better completion experience
          vim.o.completeopt = 'menuone,noselect'

          -- luasnip setup
          local luasnip = require 'luasnip'
        '';
      }
      {
        plugin = pkgs.vimPlugins.nvim-base16;
        type = "lua";
        config = with config.colorscheme.colors; ''
          require ('base16-colorscheme').setup({
            base00 = '#${base00}',
            base01 = '#${base01}',
            base02 = '#${base02}',
            base03 = '#${base03}',
            base04 = '#${base04}',
            base05 = '#${base05}',
            base06 = '#${base06}',
            base07 = '#${base07}',
            base08 = '#${base08}',
            base09 = '#${base09}',
            base0A = '#${base0A}',
            base0B = '#${base0B}',
            base0C = '#${base0C}',
            base0D = '#${base0D}',
            base0E = '#${base0E}',
            base0F = '#${base0F}',
          })
          vim.cmd [[highlight! link TelescopeSelection    Visual]]
          vim.cmd [[highlight! link TelescopeNormal       Normal]]
          vim.cmd [[highlight! link TelescopePromptNormal TelescopeNormal]]
          vim.cmd [[highlight! link TelescopeBorder       TelescopeNormal]]
          vim.cmd [[highlight! link TelescopePromptBorder TelescopeBorder]]
          vim.cmd [[highlight! link TelescopeTitle        TelescopeBorder]]
          vim.cmd [[highlight! link TelescopePromptTitle  TelescopeTitle]]
          vim.cmd [[highlight! link TelescopeResultsTitle TelescopeTitle]]
          vim.cmd [[highlight! link TelescopePreviewTitle TelescopeTitle]]
          vim.cmd [[highlight! link TelescopePromptPrefix Identifier]]
        '';
      }
      { plugin = neorg; 
        type = "lua";
        config = ''
          require'neorg'.setup {
            load = {
              ["core.defaults"] = {},
              ["core.norg.concealer"] = {},
              ["core.norg.completion"] = {
                config = { engine = "nvim-cmp" }
              },
              ["core.norg.dirman"] = {
                config = { 
                  workspaces = {
                    default = "${config.xdg.userDirs.documents}/neorg"
                  }
                }
              }
            }
          }
        '';
      }
    ];
  };
}
