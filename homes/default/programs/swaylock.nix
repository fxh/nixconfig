{ config, pkgs, ... }:
{

  programs.swaylock = with config; with config.colorscheme.colors; {
    settings = {
      clock = true;
      timestr = "%H:%M:%S";
      datestr = "%a, %b %d";

      screenshots = true;
      effect-pixelate = 10;
  
      font = font.family;
      font-size = font.size.huge;
  
      indicator-radius = 150;
      indicator-thickness = 10;
  
      text-color = "${base04}";
      text-ver-color = "00000000";
      text-wrong-color = "00000000";
      text-clear-color = "00000000";
  
      ring-color = "${base0C}";
      ring-ver-color = "${base04}";
      ring-wrong-color = "${base0B}";
      ring-clear-color = "${base0D}";
  
      key-hl-color = "${base0F}";
      bs-hl-color = "${base0B}";
  
      line-color = "00000000";
      line-ver-color = "00000000";
      line-wrong-color = "00000000";
      line-clear-color = "00000000";
  
      separator-color = "${base00}";
  
      inside-color = "00000000";
      inside-ver-color = "00000000";
      inside-wrong-color = "00000000";
      inside-clear-color = "00000000";
    };
  };
}
