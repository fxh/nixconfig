{ config, ... }:
{

  programs.vivid = {
    enable = true;
    themes = {
      base16 = {
        colors = config.colorscheme.colors;
        core = {
          normal_text = { forgeround = "base04"; };
          regular_file = { foreground = "base04"; };
          reset_to_normal = { foreground = "base04"; };
          directory = { foreground = "base0D"; font-style = "bold"; };
          symlink = { foreground = "base0E"; font-style = "bold"; };
          executable_file = { foreground = "base0F"; font-style = "bold"; };
          broken_symlink = { foreground = "base0A"; background = "base06"; font-style = "bold"; };
          missing_symlink_target = { foreground = "base0A"; background = "base06"; font-style = "bold"; };
          fifo = { foreground = "base08"; background = "base06"; font-style = "bold"; };
          socket = { foreground = "base0B"; background = "base06"; font-style = "bold"; };
          door = { foreground = "base0B"; background = "base06"; font-style = "bold"; };
          character_device = { foreground = "base08"; background = "base06"; font-style = "bold"; };
          block_device = { foreground = "base08"; background = "base06"; font-style = "bold"; };
          normal_text = { foreground = "base04"; };
          other_writable = { };
          sticky = { };
          setuid = { };
          setgid = { };
          file_with_capability = { };
          sticky_other_writable = { };
          multi_hard_link = { };
        };

        text = {
          special = { foreground = "base04"; };
          todo = { foreground = "base04"; font-style = "bold"; };
          licenses = { foreground = "base04"; };
          configuration = { foreground = "base04"; };
          other = { foreground = "base04"; };
        };

        markup = { foreground = "base04"; };
        programming = { foreground = "base04"; };
        office = { foreground = "base09"; };
        archives = { foreground = "base0A"; font-style = "bold"; };
        executable = { foreground = "base04"; };
        unimportant = { foreground = "base02"; };

        media = {
          image = { foreground = "base0B"; };
          audio = { foreground = "base0E"; };
          video = { foreground = "base0B"; font-style = "bold"; };
          fonts = { foreground = "base0C"; };
        };
      };
    };
  };
}
