{ pkgs, config, ... }:
{
  programs.zsh = {
    enable = true;
    shellAliases = { };
    enableCompletion = true;
    enableSyntaxHighlighting = true;
    enableAutosuggestions = true;
    initExtra = ''
      export LS_COLORS="$(${pkgs.vivid}/bin/vivid generate base16)"
    '';
    autocd = true;
    history = {
      size = 10000;
      path = "${config.xdg.dataHome}/zsh/history";
    };
  };
}
