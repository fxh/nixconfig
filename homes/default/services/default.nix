{ ... }:
{
  imports = [
    ./fnott.nix
    ./kanshi.nix
    ./playerctld.nix
    ./lorri.nix
    ./river.nix
    ./swayidle.nix
    ./syncthing.nix
    ./yambar.nix
  ];
}
