{ config, pkgs, ... }:
{

  services.fnott = {
    enable = true;
    settings = {
      main = with config.colorscheme.colors; with config; {
        title-font = "${font.family}:size=${font.size.small}:weight=bold";
        summary-font = "${font.family}:size=${font.size.tiny}";
        body-font = "${font.family}:size=${font.size.tiny}";
        background = "${base00}";
        border-color = "${base0C}";
        title-color = "${base06}";
        summary-color = "${base05}";
        body-color = "${base04}";

        min-width = 350;
        max-width = 350;

        edge-margin-vertical = 20;
        edge-margin-horizontal = 20;

        anchor = "top-right";
        border-size = 2;

        selection-helper = "${pkgs.fuzzel}/bin/fuzzel -d";
      };
    };
  };
}
