{ pkgs, ... }:

let 
  cauligrad = pkgs.fetchurl {
    url = "https://cdna.artstation.com/p/assets/images/images/025/400/696/medium/ismail-inceoglu-cauligrx.jpg";
    sha256 = "sha256-5N0ONBMe04CDdlt6Kjp4JR2Z7WhQqXsX1kYs+8fPhs0=";
  };

  carrotland = pkgs.fetchurl {
    url = "https://cdnb.artstation.com/p/assets/images/images/017/169/391/medium/ismail-inceoglu-carrotland.jpg";
    sha256 = "sha256-LyAPoX7TvR97tnaIEdL6Xu82ky1PAq+RAfrLBYzotVw=";
  };
in {
  services.kanshi = {
    enable = true;
    systemdTarget = "river-session.target";
    profiles = {
      laptop = {
        exec = [
          "${pkgs.swaybg}/bin/swaybg -o eDP-1 -i ${carrotland} -m fill"
        ];
        outputs = [{
          criteria = "eDP-1";
          mode = "1920x1080@60";
        }];
      };
      default = {
        exec = [
          "${pkgs.swaybg}/bin/swaybg -o \"Acer Technologies Acer X34 #ASNeW6FioRXd\" -i $HOME/pictures/wallpapers/ismail-inceoglu-mercury-in-june.jpg -m fill"
          "${pkgs.swaybg}/bin/swaybg -o \"Dell Inc. DELL U3415W 68MCF4B803YL\" -i $HOME/pictures/wallpapers/ismail-inceoglu-to-a-new-neighbourhood.jpg -m fill"
        ];
        outputs = [
          {
            criteria = "Acer Technologies Acer X34 #ASNeW6FioRXd";
            mode = "3440x1440@100";
            position = "1440,1050";
          }
          {
            criteria = "Dell Inc. DELL U3415W 68MCF4B803YL";
            mode = "3440x1440@60";
            position = "0,0";
            transform = "90";
          }
        ];
      };
    };
  };
}
