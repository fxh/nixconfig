{ config, pkgs, ... }:

let
  runFuzzel = with config.colorscheme.colors; pkgs.writeScriptBin "run-fuzzel.sh" ''
    ${pkgs.fuzzel}/bin/fuzzel \
        -f "${config.font.family}:pixelsize=${config.font.size.huge}" \
        -b "${base00}ff" \
        -t "${base06}ff" \
        -s "${base03}ff" \
        -S "${base0D}ff" \
        -m "${base0E}ff" \
        -C "${base0C}ff" \
        -P " " \
        -l 10   \
        -x 10   \
        -y 5    \
        -p 10   \
        -r 0    \
        -B 2    \
        -w 40   \
        -T ${pkgs.foot}/bin/footclient $@ <&0
    [ "$?" != "0" ] && ${pkgs.libnotify}/bin/notify-send \
        --urgency="critical" \
        --expire-time="3000" \
        --app-name="Fuzzel"  \
        --icon="fuzzel"      \
        "Error starting Application"
  '';

  decreaseBrightness = pkgs.writeScriptBin "decrease-brightness.sh" ''
    current="$(${pkgs.light}/bin/light -G)"
    outputs="$(${pkgs.wlr-randr}/bin/wlr-randr --dryrun | grep '^[^ ]\w*-[0-9]*' -o)"

    if [ "$current" == "0.00" ]; then
      for output in $outputs; do
        ${pkgs.wlr-randr}/bin/wlr-randr --output $output --off
      done
    else
      ${pkgs.light}/bin/light -U 5
    fi
  '';

  increaseBrightness = pkgs.writeScriptBin "increase-brightness.sh" ''
    current="$(${pkgs.light}/bin/light -G)"
    outputs="$(${pkgs.wlr-randr}/bin/wlr-randr --dryrun | grep '^[^ ]\w*-[0-9]*' -o)"
    allEnabled="$(${pkgs.wlr-randr}/bin/wlr-randr --dryrun | grep '^  Enabled: \K(yes|no)' -oP)"

    anyDisabled="no"
    for enabled in $allEnabled; do
      [ "$enabled" == "no" ] && anyDisabled="yes"
    done

    if [ "$anyDisabled" == "yes" ]; then
      for output in $outputs; do
        ${pkgs.wlr-randr}/bin/wlr-randr --output $output --on
      done
    else
      ${pkgs.light}/bin/light -A 5
    fi
  '';

in
{

  wayland.windowManager.river = {
    enable = true;
    settings = with config.colorscheme.colors; ''
      #!${pkgs.bash}/bin/bash
         
      riverctl background-color "0x${base00}FF"

      # Use the "logo" key as the primary modifier
      mod="Mod4"
           
      riverctl map normal $mod+Shift Return spawn "${pkgs.foot}/bin/footclient"
      riverctl map normal $mod A spawn "${runFuzzel}/bin/run-fuzzel.sh"
      riverctl map normal $mod D spawn "${pkgs.fnott}/bin/fnottctl dismiss"

      # Mod+Shift+K to close the focused view
      riverctl map normal $mod+Shift K close

      # Mod+Q to exit river
      riverctl map normal $mod+Shift Q exit

      # Mod+J and Mod+K to focus the next/previous view in the layout stack
      riverctl map normal $mod E focus-view next
      riverctl map normal $mod U focus-view previous

      # Mod+Shift+J and Mod+Shift+K to swap the focused view with the next/previous
      # view in the layout stack
      riverctl map normal $mod+Shift E swap next
      riverctl map normal $mod+Shift U swap previous

      # Mod+Period and Mod+Comma to focus the next/previous output
      riverctl map normal $mod Period focus-output next
      riverctl map normal $mod Comma focus-output previous

      # Mod+Shift+{Period,Comma} to send the focused view to the next/previous output
      riverctl map normal $mod+Shift Period send-to-output next
      riverctl map normal $mod+Shift Comma send-to-output previous

      # Mod+Return to bump the focused view to the top of the layout stack
      riverctl map normal $mod Return zoom

      # Mod+H and Mod+L to decrease/increase the main ratio of rivertile(1)
      riverctl map normal $mod N send-layout-cmd rivertile "main-ratio -0.05"
      riverctl map normal $mod I send-layout-cmd rivertile "main-ratio +0.05"

      # Mod+Shift+H and Mod+Shift+L to increment/decrement the main count of rivertile(1)
      riverctl map normal $mod+Shift N send-layout-cmd rivertile "main-count +1"
      riverctl map normal $mod+Shift I send-layout-cmd rivertile "main-count -1"

      # Mod+Alt+{H,J,K,L} to move views
      riverctl map normal $mod+Mod1 N move left 100
      riverctl map normal $mod+Mod1 E move down 100
      riverctl map normal $mod+Mod1 U move up 100
      riverctl map normal $mod+Mod1 I move right 100

      # Mod+Alt+Control+{H,J,K,L} to snap views to screen edges
      riverctl map normal $mod+Mod1+Control N snap left
      riverctl map normal $mod+Mod1+Control E snap down
      riverctl map normal $mod+Mod1+Control U snap up
      riverctl map normal $mod+Mod1+Control I snap right

      # Mod+Alt+Shif+{H,J,K,L} to resize views
      riverctl map normal $mod+Mod1+Shift N resize horizontal -100
      riverctl map normal $mod+Mod1+Shift E resize vertical 100
      riverctl map normal $mod+Mod1+Shift U resize vertical -100
      riverctl map normal $mod+Mod1+Shift I resize horizontal 100

      # Mod + Left Mouse Button to move views
      riverctl map-pointer normal $mod BTN_LEFT move-view

      # Mod + Right Mouse Button to resize views
      riverctl map-pointer normal $mod BTN_RIGHT resize-view

      for i in $(seq 1 9)
      do
          tags=$((1 << ($i - 1)))

          # Mod+[1-9] to focus tag [0-8]
          riverctl map normal $mod $i set-focused-tags $tags

          # Mod+Shift+[1-9] to tag focused view with tag [0-8]
          riverctl map normal $mod+Shift $i set-view-tags $tags

          # Mod+Ctrl+[1-9] to toggle focus of tag [0-8]
          riverctl map normal $mod+Control $i toggle-focused-tags $tags

          # Mod+Shift+Ctrl+[1-9] to toggle tag [0-8] of focused view
          riverctl map normal $mod+Shift+Control $i toggle-view-tags $tags
      done

      # Mod+0 to focus all tags
      # Mod+Shift+0 to tag focused view with all tags
      all_tags=$(((1 << 32) - 1))
      riverctl map normal $mod 0 set-focused-tags $all_tags
      riverctl map normal $mod+Shift 0 set-view-tags $all_tags

      # Mod+Space to toggle float
      riverctl map normal $mod Space toggle-float

      # Mod+F to toggle fullscreen
      riverctl map normal $mod F toggle-fullscreen

      # Mod+{Up,Right,Down,Left} to change layout orientation
      riverctl map normal $mod Up    send-layout-cmd rivertile "main-location top"
      riverctl map normal $mod Right send-layout-cmd rivertile "main-location right"
      riverctl map normal $mod Down  send-layout-cmd rivertile "main-location bottom"
      riverctl map normal $mod Left  send-layout-cmd rivertile "main-location left"

      # Declare a passthrough mode. This mode has only a single mapping to return to
      # normal mode. This makes it useful for testing a nested wayland compositor
      riverctl declare-mode passthrough

      # Mod+F11 to enter passthrough mode
      riverctl map normal $mod F11 enter-mode passthrough

      # Mod+F11 to return to normal mode
      riverctl map passthrough $mod F11 enter-mode normal

      # Various media key mapping examples for both normal and locked mode which do
      # not have a modifier
      for mode in normal locked
      do
          # Eject the optical drive
          riverctl map $mode None XF86Eject spawn 'eject -T'

          # Control pulse audio volume with pamixer (https://github.com/cdemoulins/pamixer)
          riverctl map $mode None XF86AudioRaiseVolume  spawn '${pkgs.pamixer}/bin/pamixer -i 5'
          riverctl map $mode None XF86AudioLowerVolume  spawn '${pkgs.pamixer}/bin/pamixer -d 5'
          riverctl map $mode None XF86AudioMute         spawn '${pkgs.pamixer}/bin/pamixer --toggle-mute'

          # Control MPRIS aware media players with playerctl (https://github.com/altdesktop/playerctl)
          riverctl map $mode None XF86AudioMedia spawn '${pkgs.playerctl}/bin/playerctl play-pause'
          riverctl map $mode None XF86AudioPlay  spawn '${pkgs.playerctl}/bin/playerctl play-pause'
          riverctl map $mode None XF86AudioPrev  spawn '${pkgs.playerctl}/bin/playerctl previous'
          riverctl map $mode None XF86AudioNext  spawn '${pkgs.playerctl}/bin/playerctl next'

          # Control screen backlight brighness with light (https://github.com/haikarainen/light)
          riverctl map $mode None XF86MonBrightnessUp   spawn '${increaseBrightness}/bin/increase-brightness.sh'
          riverctl map $mode None XF86MonBrightnessDown spawn '${decreaseBrightness}/bin/decrease-brightness.sh'
      done

      # Set background and border color 
      riverctl border-color-focused "0x${base0B}FF"
      riverctl border-color-unfocused "0x${base03}FF" 
      riverctl border-color-urgent "0x${base0C}FF"

      # Set border width 
      riverctl border-width 2

      # Set repeat rate
      riverctl set-repeat 80 300

      # Make certain views start floating
      riverctl float-filter-add title "Firefox — Sharing Indicator"

      riverctl default-layout rivertile

      riverctl input 1133:16500:Logitech_G305 accel-profile none

      # Middle click with three finger tap
      riverctl input pointer-1739-52861-SYNA32B3:01_06CB:CE7D_Touchpad middle-emulation enabled
      riverctl input pointer-1739-52861-SYNA32B3:01_06CB:CE7D_Touchpad tap enabled

      # Disable trackpad while typing
      riverctl input 1739:52861:SYNA32B3:01_06CB:CE7D_Touchpad disable-while-typing enabled
    '';
  };
}
