{ config, ... }:

let 
  homeDir = config.home.homeDirectory;
  documentsDir = config.xdg.userDirs.documents;
  absDocs = builtins.replaceStrings ["$HOME"] [homeDir] documentsDir;

in {
  services.syncthing = {
    enable = true;

    folders = {
      "${absDocs}/repositories" = {
        id = "repositories";
        versioning = {
          type = "simple";
          params.keep = "50";
        };
        devices = [ "phone" ];
      };
    };
    devices = {
      phone = {
        id = "MTQPSDB-FEXJGJL-5PQQDGH-7WAHEPL-NQA467J-6KM4RQF-7YY3L5Q-A4CLGAU";
      };
    };
  };
}
