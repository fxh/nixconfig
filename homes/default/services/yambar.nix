{ config, pkgs, ... }:
let
  yambarVars = {
    default = {
      foreground = "${config.colorscheme.colors.base04}ff";
      margin = 12;
      deco = {
        background = {
          color = "${config.colorscheme.colors.base01}ff";
        };
      };
    };

    underlined = yambarVars.default // {
      foreground = "${config.colorscheme.colors.base06}ff";
      deco = {
        stack = [
          { background = { color = "${config.colorscheme.colors.base01}ff"; }; }
          { underline = { size = 3; color = "${config.colorscheme.colors.base0C}ff"; }; }
        ];
      };
    };

    focused = yambarVars.default // {
      foreground = "${config.colorscheme.colors.base06}ff";
      deco = {
        stack = [
          { background = { color = "${config.colorscheme.colors.base03}ff"; }; }
          { underline = { size = 3; color = "${config.colorscheme.colors.base0C}ff"; }; }
        ];
      };
    };
  };

  yambarConfig = {

    bar = {
      font = "${config.font.family}:pixelsize=${config.font.size.big}";
      height = 35;
      spacing = 15;
      margin = 0;
      location = "bottom";
      foreground = "${config.colorscheme.colors.base06}ff";
      background = "${config.colorscheme.colors.base00}ff";

      border = {
        width = 2;
        color = "${config.colorscheme.colors.base03}ff";
        margin = 10;
        top-margin = 0;
      };

      right = [
        {
          clock = {
            date-format = "%a, %b %d";
            content = { string = { text = "{date} - {time}"; } // yambarVars.underlined; };
          };
        }
      ];

      left = [
        {
          river = {
            content = {
              map = {
                tag = "state";
                values = {
                  focused = { string = { text = "{id}"; } // yambarVars.focused; };
                };
                default = {
                  map = {
                    tag = "occupied";
                    values = {
                      true = { string = { text = "{id}"; } // yambarVars.default; };
                    };
                  };
                };
              };
            };
          };
        }
      ];
    };
  };

in
{

  services.yambar = {
    enable = true;
    settings = yambarConfig;
  };

}
