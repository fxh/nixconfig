{ config, pkgs, ... }:
let
  domain = "shgy.xyz";
  email = "flyxi@shgy.xyz";
in {

  time.timeZone = "Europe/Berlin";

  networking = {
    useDHCP = false;
    hostName = "arceus";
    useNetworkd = true;

    firewall = {
      enable = true;
      trustedInterfaces = [ config.services.tailscale.interfaceName ];
      allowedTCPPorts = [ 22 80 443 ];
      allowedUDPPorts = [ config.services.tailscale.port ];
    };

    interfaces = { 
      ens3 = {
        useDHCP = true; 

        ipv4.addresses = [
          { address = "95.216.177.87"; prefixLength = 32; }
        ];
      };
      ens10.useDHCP = true;
    };
  };

  sops = {
    defaultSopsFile = ./secrets/secrets.json;

    secrets = {
      headscale-private-key = {
        owner = "headscale";
      };
      gcp-restic = { };
      restic-password = { };
    };
  };


  services = {
    tailscale.enable = true;

    tailscale-autoconnect = {
      enable = true;
      loginServer = config.services.headscale.serverUrl;
      namespace = "test";
      headscale-integration = true;
    };

    openssh = {
      enable = true;
      permitRootLogin = "prohibit-password";
    };

    restic = {
      backups = {
        gcp = {
          repository = "gs:restic-339504:/";
          paths = [
            "/var/lib/headscale/db.sqlite"
          ];
          initialize = true;
          passwordFile = config.sops.secrets.restic-password.path;
          environmentFile = toString (pkgs.writeText "restic-env" ''
            GOOGLE_PROJECT_ID=terraform-339504
            GOOGLE_APPLICATION_CREDENTIALS=${config.sops.secrets.gcp-restic.path}
          '');
        };
      };
    };

    headscale = {
      enable = true;

      serverUrl = "https://headscale.${domain}";

      privateKeyFile = config.sops.secrets.headscale-private-key.path;

      settings = {
        grpc_listen_addr = "127.0.0.1:50443";
        grpc_allow_insecure = true;

        ip_prefixes = [
          "fd7a:115c:a1e0::/48"
          "100.64.0.0/10"
        ];
      };
    };

    traefik = {
      enable = true;
      staticConfigOptions = {
        api = { dashboard = true; };
        log.level = "DEBUG";

        entryPoints = {
          web = {
            address = ":80";
            http.redirections.entryPoint = {
              to = "websecure";
              scheme = "https";
            };
          };
          websecure = {
            address = ":443";
          };
        };

        certificatesResolvers.letsencrypt.acme = {
          inherit email;
          storage = "${config.services.traefik.dataDir}/acme.json";
          keyType = "EC256";
          tlsChallenge = { };
        };
      };

      dynamicConfigOptions.http = {
        routers = let
          tls.certresolver = "letsencrypt";
          entrypoints = [ "websecure" ];
        in {
          headscale = {
            inherit tls entrypoints;
            rule = "Host(`headscale.${domain}`)";
            service = "headscale";
          };
          headscale-grpc = {
            inherit tls entrypoints;
            rule = "Host(`headscale.${domain}`) && PathPrefix(`/headscale`)";
            service = "headscale-grpc";
          };
          api = {
            inherit tls entrypoints;
            rule = "Host(`traefik.${domain}`)";
            service = "api@internal";
          };
        };
        services = with config.services.headscale; {
          headscale.loadBalancer = {
            servers = [ 
              { url =  "http://127.0.0.1:${toString port}"; } 
            ];
          };
          headscale-grpc.loadBalancer = {
            servers = [
              { url = "h2c://${settings.grpc_listen_addr}"; } 
            ];
          };
        };
      };
    };
  };

  system.stateVersion = "21.11";
}
