{ config, pkgs, ... }:

{
  time.timeZone = "Europe/Berlin";

  programs = { neovim.enable = true; };

  networking = {
    useDHCP = false;

    useNetworkd = true;
    interfaces = { 
      ens3.useDHCP = true; 
    };
  };

  services = {
    openssh = {
      enable = true;
      permitRootLogin = "prohibit-password";
    };

    cloud-init = {
      enable = true;
      network.enable = true;
      config = ''
        system_info:
          distro: nixos

        users:
           - root

        network:
          config: disabled

        disable_root: false
        preserve_hostname: false

        cloud_init_modules:
         - migrator
         - seed_random
         - bootcmd
         - write-files
         - growpart
         - resizefs
        # - update_etc_hosts
         - ca-certs
         - rsyslog
         - users-groups

        cloud_config_modules:
         - disk_setup
         - mounts
         - ssh-import-id
         - set-passwords
         - timezone
         - disable-ec2-metadata
         - runcmd
         - ssh

        cloud_final_modules:
         - rightscale_userdata
         - scripts-vendor
         - scripts-per-once
         - scripts-per-boot
         - scripts-per-instance
         - scripts-user
         - ssh-authkey-fingerprints
         - keys-to-console
         - phone-home
         - final-message
         - power-state-change
      '';
    };
  };

  environment = { systemPackages = with pkgs; [ wget ]; };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?
}
