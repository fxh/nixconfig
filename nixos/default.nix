{ self
, nixpkgs
, sops-nix
, home-manager
, nix-colors
, nur
, neovim-nightly
, stable
, unstable
, master
, ...
}:

let

  nixosSystem = nixpkgs.lib.nixosSystem;

  nixPathModule = {
    nix.nixPath = [
      "nixpkgs=${nixpkgs}"
      "home-manager=${home-manager}"
      "master=${master}"
      "unstable=${unstable}"
      "stable=${stable}"
    ];
  };

  baseHomeManagerModule = {
    home-manager = {

      useGlobalPkgs = true;
      useUserPackages = true;

      sharedModules = [
        nix-colors.homeManagerModule
        ./modules/home-manager/services
        ./modules/home-manager/programs
        ./modules/home-manager/misc
      ];

    };
  };

  hetznerModules = [
    {
      nix.nixPath = [
        "nixpkgs=${nixpkgs}"
      ];
    }
    sops-nix.nixosModules.sops
    ./modules/hardware/hetzner-cloud.nix
    ./modules/nixpkgs
    ./modules/system
    ./modules/services
  ];

  defaultModules = [
    home-manager.nixosModules.home-manager
    sops-nix.nixosModules.sops
    nixPathModule
    ./modules/nixpkgs
    ./modules/system
    ./modules/services
  ];

in {

  shiggy =  
  let 
    colorscheme = nix-colors.colorSchemes.nord;
    homeModule = nixpkgs.lib.recursiveUpdate baseHomeManagerModule  {
      home-manager.users.flyxi = import ../homes/default;
      home-manager.extraSpecialArgs = { inherit colorscheme nix-colors; };
    };
  in nixosSystem {
    system = "x86_64-linux";

    specialArgs = { inherit nixpkgs stable unstable master nur neovim-nightly colorscheme; };

    modules = defaultModules ++ [
      homeModule
      ./shiggy
    ];
  };

  base-server = nixosSystem {
    system = "x86_64-linux";

    specialArgs = { inherit nixpkgs stable unstable master nur neovim-nightly; };

    modules = hetznerModules ++ [ ./base-server ];
  };

  arceus = nixosSystem {
    system = "x86_64-linux";

    specialArgs = { inherit nixpkgs stable unstable master nur neovim-nightly; };

    modules = hetznerModules ++ [ ./arceus ];
  };
}
