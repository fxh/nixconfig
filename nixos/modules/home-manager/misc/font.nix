{ config, lib, ... }:
with lib;
{
  options.font = {
    family = mkOption {
      type = types.str;
    };

    size = mkOption {
      type = types.anything;
    };
  };
}
