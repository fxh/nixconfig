{ config, lib, ... }:

with lib;

{
  options.profile = {
    name = mkOption {
      type = types.str;
    };

    sshKey = mkOption {
      type = types.submodule {
        options = {
          pub = mkOption {
            type = types.str;
          };
          name = mkOption {
            type = types.str;
          };
        };
      };
    };

    u2fKey = mkOption {
      type = types.str;
    };

    email = mkOption {
      type = types.str;
    };
  };
}
