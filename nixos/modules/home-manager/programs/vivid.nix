{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.programs.vivid;
  yamlFormat = pkgs.formats.yaml { };
in
{
  options = {
    programs.vivid = {
      enable = mkEnableOption "Vivid";

      package = mkOption {
        type = types.package;
        default = pkgs.vivid;
        defaultText = literalExpression "pkgs.vivid";
        description = "The Package to use for vivid";
      };

      themes = mkOption {
        type = types.attrsOf yamlFormat.type;
        default = { };
      };
    };
  };

  config = mkIf cfg.enable {

    home.packages = [ cfg.package ];

    xdg.configFile = builtins.listToAttrs (mapAttrsToList
      (name: content: {
        name = "vivid/themes/${name}.yml";
        value = {
          text = builtins.toJSON content;
        };
      })
      cfg.themes);
  };
}

