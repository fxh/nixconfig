{ ... }:
{
  imports = [
    ./fnott.nix
    ./swayidle.nix
    ./syncthing.nix
    ./yambar.nix
    ./windowManager/river.nix
  ];
}
