{ pkgs, lib, config, ... }:

with lib;

let
  cfg = config.wayland.windowManager.river;
in
{
  options = {
    wayland.windowManager.river = {
      enable = mkEnableOption "River, a dynamic wayland compositor";

      package = mkOption {
        type = types.package;
        default = pkgs.river;
        example = literalExample "pkgs.river";
        description = "The River package to install";
      };

      settings = mkOption {
        type = types.lines;
        default = null;
        example = literalExample ''
        '';
        description = "Configuration";
      };

      systemdIntegration = mkOption {
        type = types.bool;
        default = pkgs.stdenv.isLinux;
        example = false;
        description = ''
          Whether to enable <filename>river-session.target</filename> on
          river startup. This links to
          <filename>graphical-session.target</filename>.
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    home.packages = [ cfg.package ];

    xdg.configFile."river/init" = mkIf (cfg.settings != "") {
      executable = true;
      text = cfg.settings + (concatStringsSep "\n" (
        (optional cfg.systemdIntegration ''
          systemctl --user import-environment &
          systemctl --user start river-session.target &

          trap "systemctl --user stop graphical-session.target" SIGTERM

          sleep infinity
        '')
      ));
    };

    systemd.user.services.rivertile = mkIf cfg.systemdIntegration {
      Unit = {
        Description = "Tiling for river";
        PartOf = [ "river-session.target" ];
        After = [ "river-session.target" ];
      };

      Service = {
        ExecStart = "${cfg.package}/bin/rivertile";
        Restart = "on-failure";
      };

      Install = { WantedBy = [ "river-session.target" ]; };
    };

    systemd.user.targets.river-session = mkIf cfg.systemdIntegration {
      Unit = {
        Description = "river compositor session";
        BindsTo = [ "graphical-session.target" ];
        Wants = [ "graphical-session-pre.target" ];
        After = [ "graphical-session-pre.target" ];
      };
    };
  };
}
