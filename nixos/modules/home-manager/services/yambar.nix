{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.yambar;
  yamlFormat = pkgs.formats.yaml { };
in
{
  options = {
    services.yambar = {
      enable = mkEnableOption "Yambar";

      package = mkOption {
        type = types.package;
        default = pkgs.yambar;
        defaultText = literalExpression "pkgs.yambar";
        description = "The Yambar package for the service.";
      };

      settings = mkOption {
        type = yamlFormat.type;
        default = { };
        description = ''
          Configuration written to
          <filename>$XDG_CONFIG_HOME/yambar/config.yml</filename>. See
          <link xlink:href="https://codeberg.org/dnkl/yambar/src/branch/master/examples/configurations"/>
          for example configurations.
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.user.services.yambar = {
      Unit = {
        Description = "Yambar";
        PartOf = [ "graphical-session.target" ];
        After = [ "graphical-session.target" ];
      };

      Install = {
        WantedBy = [ "graphical-session.target" ];
      };

      Service = {
        ExecStart = "${cfg.package}/bin/yambar";
        Restart = "on-failure";
      };
    };

    xdg.configFile."yambar/config.yml" = mkIf (cfg.settings != { }) {
      text = builtins.toJSON cfg.settings;
    };
  };
}

