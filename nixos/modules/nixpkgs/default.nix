{ config, nixpkgs, stable, unstable, master, nur, neovim-nightly, ... }:

let
  channelOverlay = final: prev: {
    stable = import stable { inherit (prev) system; };
    unstable = import unstable { inherit (prev) system; };
    master =import master { inherit (prev) system; };
  };

in {
  nixpkgs = {


    config.allowUnfree = true;

    config.allowUnfreePredicate = pkg: builtins.elem (nixpkgs.lib.getName pkg) [
      "steam"
      "steam-original"
      "steam-runtime"
    ];

    overlays = [
      nur.overlay
      neovim-nightly.overlay

      channelOverlay

      (import ./overlays/river.nix)
      (import ./overlays/headscale.nix)
      (import ./overlays/swaylock-effects.nix)
      (import ./overlays/remarshal.nix)
      (import ./overlays/fnott.nix)
      (import ./overlays/foot.nix)
      (import ./overlays/run-river.nix)
    ];
  };
}

