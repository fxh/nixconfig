final: prev: {
  foot = prev.foot.overrideAttrs (old: {
    postInstall = ''
      ${old.postInstall}
      rm "$out/share/applications/foot-server.desktop"
    '';
  });
}
