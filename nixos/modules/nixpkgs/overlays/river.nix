final: prev: {
  river = prev.river.overrideAttrs (old: {
    version = "0.2.0-dev-touch";
    src = prev.fetchFromGitHub {
      owner = "fhilgers";
      repo = "river";
      rev = "4ee89bb9c722c7453ad315ae095bcd316463a494";
      sha256 = "sha256-9KShfpddJm5L8hdDOzK8YigiTEb90nUwaPufpOxjq3k=";
      fetchSubmodules = true;
    };
  });
}
