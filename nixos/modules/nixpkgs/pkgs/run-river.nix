{ pkgs }:

pkgs.writeShellScriptBin "run-river.sh" ''
  export _JAVA_AWT_WM_NONREPARENTING=1
  export XKB_DEFAULT_LAYOUT="us"
  export XKB_DEFAULT_VARIANT="colemak_dh"
  export XKB_DEFAULT_OPTIONS="ctrl:swapcaps"
  exec "${pkgs.river}/bin/river"
''
