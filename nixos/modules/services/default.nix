{ ... }:

{
  imports = [
    ./tailscale-autoconnect.nix
    ./seatd.nix
  ];
}
