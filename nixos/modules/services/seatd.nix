{ pkgs, config, lib, ... }:

with lib;

let
  cfg = config.services.seatd;
in
{
  options = {
    services.seatd = {
      enable = mkEnableOption "seatd";

      package = mkOption {
        type = types.package;
        default = pkgs.seatd;
        example = literalExample "pkgs.seatd";
        description = "The Seatd package to be installed";
      };

      group = mkOption {
        type = types.str;
        default = "seatd";
        example = iteralExample "seatd";
        description = "The group name that will have access to seatd";
      };
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [ cfg.package ];

    users.groups = mkIf (cfg.group == "seatd") {
      seatd.name = "seatd";
    };

    systemd.services.seatd = {
      path = [ pkgs.bash ];

      serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.seatd}/bin/seatd -g ${cfg.group}";
        Group = cfg.group;
        Restart = "on-failure";
      };

      wantedBy = [ "multi-user.target" ];
    };
  };
}
