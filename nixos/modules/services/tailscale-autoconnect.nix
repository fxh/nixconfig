{ pkgs, config, lib, ... }:

with lib;

let
  cfg = config.services.tailscale-autoconnect;
in
{
  options = {
    services.tailscale-autoconnect = {
      enable = mkEnableOption "tailscale-autoconnect";
      hostName = mkOption {
        type = types.nullOr types.str;
        default = null;
      };

      loginServer = mkOption {
        type = types.nullOr types.str;
        default = null;
      };

      authKeyFile = mkOption {
        type = types.nullOr types.str;
        default = null;
      };

      headscale-integration = mkOption {
        type = types.bool;
        default = false;
      };

      namespace = mkOption {
        type = types.nullOr types.str;
        default = null;
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services.tailscale-autoconnect = {
      description = "Automatic connection to Tailscale";

      after = [ "network-pre.target" "tailscaled.service" ] ++ optional (cfg.headscale-integration) "headscale.service";
      wants = [ "network-pre.target" "tailscaled.service" ] ++ optional (cfg.headscale-integration) "headscale.service";
      wantedBy = [ "multi-user.target" ];

      serviceConfig.Type = "oneshot";

      script = 
        let 
          createNamespace = "${pkgs.headscale}/bin/headscale namespaces create ${cfg.namespace}";
          createKey = "${pkgs.headscale}/bin/headscale preauthkeys create -n ${cfg.namespace}";

          upCommand = "${pkgs.tailscale}/bin/tailscale up";

          loginCommand = builtins.concatStringsSep " " ([
            "${pkgs.tailscale}/bin/tailscale up"
            "--accept-dns=false"
            (
              if (cfg.headscale-integration) 
              then "--authkey $(${createNamespace} > /dev/null 2>&1 && ${createKey})"
              else "--authkey $(cat ${cfg.authKey})"
            )
          ]
          ++ optional (cfg.loginServer != null) "--login-server ${cfg.loginServer}"
          ++ optional (cfg.hostName != null) "--hostname ${cfg.hostName}");

        in
        ''
          # wait for tailscaled to settle
          sleep 2
          # check if we are already authenticated to tailscale
          status="$(${pkgs.tailscale}/bin/tailscale status -json | ${pkgs.jq}/bin/jq -r .BackendState)"
          if [ $status = "Running" ]; then # if so, then do nothing
            exit 0
          elif [ $status = "Stopped" ]; then
            ${upCommand}
          else
            ${loginCommand}
          fi
        '';
    };
  };
}
