# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, colorscheme, ... }:

{

  imports = [ ./hardware-configuration.nix ];

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
  };


  xdg.portal.wlr = {
    enable = true;
    settings = {
      screencast = {
        max_fps = 60;
        chooser_cmd = "${pkgs.slurp}/bin/slurp -f %o -or";
      };
    };
  };


  nix = {
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';

    settings.allowed-users = [ "flyxi" ];

    gc = {
      automatic = true;
      dates = "weekly";
    };
  };

  sops = {
    defaultSopsFile = ./secrets/secrets.json;

    secrets.wpa-supplicant = {
      mode = "0400";
      path = "/etc/wpa_supplicant.conf";
    };
  };


  users.users.flyxi = {
    isNormalUser = true;
    extraGroups = [
      "wheel"
      "video"
      "seatd"
    ];
    shell = pkgs.zsh;
  };

  programs = {
    ssh.startAgent = true;
    dconf.enable = true;
    light.enable = true;
    steam.enable = true;
    gamemode = {
      enable = true;
      settings = {
        general = {
          renice = 10;
          inhibit_screensaver = 0;
        };
        # Warning: GPU optimisations have the potential to damage hardware
        #gpu = {
        #  apply_gpu_optimisations = "accept-responsibility";
        #  gpu_device = 0;
        #  amd_performance_level = "high";
        #};
        custom = {
          start = "''${pkgs.libnotify}/bin/notify-send 'GameMode started'";
          end = "''${pkgs.libnotify}/bin/notify-send 'GameMode ended'";
        };
      };
    };
  };

  security = {
    rtkit.enable = true;
    pam = {
      services.swaylock = { };
      u2f.enable = true;
    };
  };

  services = {
    pipewire = {
      enable = true;
      alsa.enable = true;
      pulse.enable = true;
    };

    tailscale.enable = true;
    seatd.enable = true;

    openssh = {
      enable = true;
      permitRootLogin = "no";
    };

    greetd = {
      enable = true;
      vt = 7;
      settings = {
        default_session = {
          command = "${pkgs.greetd.tuigreet}/bin/tuigreet --time --cmd ${pkgs.runRiver}/bin/run-river.sh";
        };
      };
    };

    xserver = {
      layout = "us";
      xkbVariant = "colemak_dh";
    };
  };

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    earlySetup = true;
    font = "${pkgs.spleen}/share/consolefonts/spleen-12x24.psfu";
    colors = lib.mapAttrsToList (_: value: value) colorscheme.colors;
    useXkbConfig = true;
  };

  networking = {
    hostName = "shiggy";
    wireless.enable = true;

    interfaces = {
      wlp2s0.useDHCP = true;
    };

    firewall = {
      allowedUDPPorts = [
        config.services.tailscale.port

        # syncthing
        22000
        21027
      ];
      allowedTCPPorts = [
        # syncthing
        22000
      ];
      trustedInterfaces = [ config.services.tailscale.interfaceName ];
      enable = true;
    };
  };

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  fonts = {
    fonts = with pkgs; [
      (nerdfonts.override {
        fonts = [
          "Iosevka"
        ];
      })

      noto-fonts
      noto-fonts-extra
      noto-fonts-cjk
      noto-fonts-emoji

      font-awesome
      material-design-icons
    ];
  };


  hardware = {
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
    };

    pulseaudio.enable = false;
    bluetooth.enable = true;

    video.hidpi.enable = lib.mkDefault true;
  };

  documentation.nixos.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?
}
